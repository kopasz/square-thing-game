using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace NiceGame
{
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		//Drawing
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;

		//World
		World m_World;

		//Sprites
		Player m_Player;
		Cursor m_Cursor;
		Sprite m_Bg;

		public Game1()
		{
			graphics = new GraphicsDeviceManager( this );
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			graphics.PreferMultiSampling = true;

			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferWidth = 800;
			graphics.PreferredBackBufferHeight = 600;
			graphics.ApplyChanges();

			m_World = new World();
			m_World.SetScreen( GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height );
			m_World.GenerateNewRoom( graphics.GraphicsDevice );

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = new SpriteBatch( GraphicsDevice );

			//Sprites
			m_Player = new Player( m_World, Content.Load<Texture2D>( "pebble" ), new Vector2( m_World.GetScreenWidth() / 2, m_World.GetScreenHeight() / 2 ) );
			m_Cursor = new Cursor( Content.Load<Texture2D>( "cursor" ) );
			m_Bg = new Sprite( m_World, Content.Load<Texture2D>( "cloud2" ), new Vector2( 0.0f, 0.0f ) );

			//m_World.m_Line = new Texture2D( GraphicsDevice, 1, 1 );
			//m_World.m_Line.SetData<Color>( new Color[] { Color.White } );
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		bool bTest;
		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update( GameTime gameTime )
		{
			m_Player.Update();
			m_Cursor.Update();

			UpdateCamera(); //Camera position

			//TEST
			if( Keyboard.GetState().IsKeyDown( Keys.E ) && bTest == false )
			{
				bTest = true;
				m_World.GenerateNewRoom( graphics.GraphicsDevice );
			}
			else if( Keyboard.GetState().IsKeyUp( Keys.E ) )
				bTest = false;

			base.Update( gameTime );
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw( GameTime gameTime )
		{
			GraphicsDevice.Clear( Color.Black );
			
			//HUD and stuff that shouldn't scroll
			spriteBatch.Begin();
			m_Cursor.Draw( spriteBatch );
			spriteBatch.End();

			//Scrolling world
			spriteBatch.Begin( SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullNone, null, Matrix.CreateTranslation( -m_World.m_vCameraPos.X, -m_World.m_vCameraPos.Y, 0.0f ) );
			m_Player.Draw( spriteBatch );
			//m_Bg.Draw( spriteBatch );
			for( int i = 0; i < m_World.m_aWall.Length; ++i )
				m_World.m_aWall[i].Draw( spriteBatch );
			m_World.m_Door.Draw( spriteBatch );
			spriteBatch.End();

			base.Draw( gameTime );
		}

		private void UpdateCamera()
		{
			//Map boundaries
			float fCameraOffsetX = spriteBatch.GraphicsDevice.Viewport.Width * 0.5f;
			float fWallLeft = m_World.m_vCameraPos.X + fCameraOffsetX;
			float fWallRight = m_World.m_vCameraPos.X + spriteBatch.GraphicsDevice.Viewport.Width - fCameraOffsetX;

			float fCameraOffsetY = spriteBatch.GraphicsDevice.Viewport.Height * 0.5f;
			float fWallTop = m_World.m_vCameraPos.Y + fCameraOffsetY;
			float fWallBot = m_World.m_vCameraPos.Y + spriteBatch.GraphicsDevice.Viewport.Height - fCameraOffsetY;

			//Horizontal camera pos
			float fMoveX = 0.0f, fMoveY = 0.0f;
			if( m_Player.GetPos().X < fWallLeft )
				fMoveX = m_Player.GetPos().X - fWallLeft;
			else if( m_Player.GetPos().X > fWallRight )
				fMoveX = m_Player.GetPos().X - fWallRight;

			//Vertical camera pos
			if( m_Player.GetPos().Y < fWallBot )
				fMoveY = m_Player.GetPos().Y - fWallBot;
			else if( m_Player.GetPos().Y > fWallTop )
				fMoveY = m_Player.GetPos().Y - fWallTop;

			//Update camera pos with min and max values
			m_World.m_vCameraPos = new Vector2( MathHelper.Clamp( m_World.m_vCameraPos.X + fMoveX, -1500.0f, 1500.0f - spriteBatch.GraphicsDevice.Viewport.Width ), MathHelper.Clamp( m_World.m_vCameraPos.Y + fMoveY, -1500.0f, 1500.0f + spriteBatch.GraphicsDevice.Viewport.Height ) );
		}
	}
}

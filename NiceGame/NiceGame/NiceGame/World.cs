﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace NiceGame
{
	class World
	{
		Random rand;

		//Screen and graphics
		private int m_nScreenWidth, m_nScreenHeight;
		public Vector2 m_vCameraPos;

		//Level
		public Line[] m_aWall;
		public Line m_Door;

		private const int DOOR_SIZE = 60;

		/// <summary>
		/// World constructor
		/// </summary>
		public World()
		{
			rand = new Random();
			m_nScreenWidth = m_nScreenWidth = 0;
			m_vCameraPos = new Vector2();

			m_aWall = new Line[4];
		}

		/// <summary>
		/// Set new height and width for the game screen
		/// </summary>
		/// <param name="x">Width</param>
		/// <param name="y">Height</param>
		public void SetScreen( int x, int y )
		{
			m_nScreenWidth = x;
			m_nScreenHeight = y;
		}

		public int GetScreenWidth()
		{
			return m_nScreenWidth;
		}

		public int GetScreenHeight()
		{
			return m_nScreenHeight;
		}

		/// <summary>
		/// Returns the virtual in-game mouse position, relative to camera position
		/// </summary>
		public Vector2 GetRelativeMousePos()
		{
			MouseState mouse = Mouse.GetState();
			return new Vector2( mouse.X + m_vCameraPos.X, mouse.Y + m_vCameraPos.Y );
		}

		/// <summary>
		/// New random sized room
		/// </summary>
		public void GenerateNewRoom( GraphicsDevice device )
		{
			//Spawn walls
			int nHeight = rand.Next(400, 400);
			int nWidth = rand.Next(400, 400);
			int nStart = 50;

			m_aWall[0] = new Line( device, new Vector2( nStart, nStart + nHeight ),				new Vector2( nStart, nStart ) );
			m_aWall[1] = new Line( device, new Vector2( nStart, nStart ),						new Vector2( nStart + nWidth, nStart ) );
			m_aWall[2] = new Line( device, new Vector2( nStart + nWidth, nStart ),				new Vector2( nStart + nWidth, nStart + nHeight ) );
			m_aWall[3] = new Line( device, new Vector2( nStart + nWidth, nStart + nHeight ),	new Vector2( nStart, nStart + nHeight ) );

			//Spawn door
			int nWall = rand.Next( 0, m_aWall.Length ); //Random wall
			
			float fSideOpposite = DOOR_SIZE * (float)Math.Sin( m_aWall[nWall].GetAngle() );
			float fSideBase = (float)Math.Sqrt( Math.Pow( DOOR_SIZE, 2 ) - Math.Pow( fSideOpposite, 2 ) ); //Pythagoras
			
			int nMinX = Math.Min( Math.Min( (int)m_aWall[nWall].m_vStart.X + DOOR_SIZE, (int)m_aWall[nWall].m_vEnd.X + DOOR_SIZE ), Math.Max( (int)m_aWall[nWall].m_vStart.X, (int)m_aWall[nWall].m_vEnd.X ) );
			int nMaxX = Math.Max( Math.Min( (int)m_aWall[nWall].m_vStart.X, (int)m_aWall[nWall].m_vEnd.X ), Math.Max( (int)m_aWall[nWall].m_vStart.X - DOOR_SIZE, (int)m_aWall[nWall].m_vEnd.X - DOOR_SIZE ) );
			int nMinY = Math.Min( Math.Min( (int)m_aWall[nWall].m_vStart.Y + DOOR_SIZE, (int)m_aWall[nWall].m_vEnd.Y + DOOR_SIZE ), Math.Max( (int)m_aWall[nWall].m_vStart.Y, (int)m_aWall[nWall].m_vEnd.Y ) );
			int nMaxY = Math.Max( Math.Min( (int)m_aWall[nWall].m_vStart.Y, (int)m_aWall[nWall].m_vEnd.Y ), Math.Max( (int)m_aWall[nWall].m_vStart.Y - DOOR_SIZE, (int)m_aWall[nWall].m_vEnd.Y - DOOR_SIZE ) );
			
			Vector2 vStart = new Vector2( rand.Next( nMinX, nMaxX ), rand.Next( nMinY, nMaxY ) ); //Wall start point
			m_Door = new Line( device, vStart, new Vector2( vStart.X + fSideBase, vStart.Y + fSideOpposite ), 1 );
			m_Door.SetColor( Color.LawnGreen );
		}
	}

	/// <summary>
	/// Drawing lines
	/// </summary>
	class Line
	{
		private Texture2D m_Texture;
		private Rectangle m_Rect;
		private float m_fAngle;
		private Color m_Color;

		public Vector2 m_vStart;
		public Vector2 m_vEnd;

		/// <summary>
		/// New line constructor
		/// </summary>
		/// <param name="vStart">Line begin coordinate</param>
		/// <param name="vEnd">Line end coordinate</param>
		/// <param name="nThickness">Line thickness</param>
		public Line( GraphicsDevice device, Vector2 vStart, Vector2 vEnd, int nThickness = 1 )
		{
			m_vStart = vStart;
			m_vEnd = vEnd;
			
			m_fAngle = GetAngle();
			m_Rect = new Rectangle( (int)vStart.X, (int)vStart.Y, (int)(vEnd - vStart).Length(), nThickness );

			m_Texture = new Texture2D( device, 1, 1 );
			m_Texture.SetData<Color>( new Color[] { Color.White } );

			m_Color = Color.Purple;
		}

		public void Draw( SpriteBatch spriteBatch )
		{
			spriteBatch.Draw( m_Texture, m_Rect, null, m_Color, m_fAngle, new Vector2( 0, 0 ), SpriteEffects.None, 0 );
		}

		public float GetAngle()
		{
			Vector2 v = m_vEnd - m_vStart;
			return (float)Math.Atan2( v.Y, v.X );
		}

		public void SetColor( Color color )
		{
			m_Color = color;
		}
	}
}

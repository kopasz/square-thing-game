﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace NiceGame
{
	class Sprite
	{
		protected World m_World;

		protected Texture2D m_Texture;
		protected Vector2 m_vPos;
		protected Rectangle m_Rect;
		protected float m_fAngle;
		
		//Sprite constructor
		public Sprite( World world, Texture2D texture, Vector2 vPos )
		{
			m_World = world;
			m_Texture = texture;
			m_vPos = vPos;

			m_Rect = new Rectangle( (int)Math.Round( m_vPos.X ), (int)Math.Round( m_vPos.Y ), m_Texture.Width, m_Texture.Height );
		}

		public virtual void Draw( SpriteBatch spriteBatch )
		{
			spriteBatch.Draw( m_Texture, m_Rect, Color.White );
		}

		public virtual void Update()
		{
			//Update rectangle position
			m_Rect.X = (int)Math.Round( m_vPos.X );
			m_Rect.Y = (int)Math.Round( m_vPos.Y );
		}

		public Vector2 GetPos()
		{
			return m_vPos;
		}
	}

	class Player : Sprite
	{
		private const float ACCELERATE_SPEED = 1.1f;
		private const float MAX_SPEED = 6.0f;

		private float m_fSpeedX;
		private float m_fSpeedY;

		public Player( World world, Texture2D texture, Vector2 vPos )
			:base( world, texture, vPos )
		{
			m_fSpeedX = m_fSpeedY = 0.0f;
		}

		public override void Update()
		{
			//Set angle towards mouse pointer
			m_fAngle = (float)( Math.Atan2( m_World.GetRelativeMousePos().Y - m_vPos.Y, m_World.GetRelativeMousePos().X - m_vPos.X ) );

			//Acceleration and movement
			KeyboardState keyboard = Keyboard.GetState();
			if( keyboard.IsKeyDown( Keys.W ) )
				m_fSpeedY -= ACCELERATE_SPEED;
			else if( keyboard.IsKeyDown( Keys.S ) )
				m_fSpeedY += ACCELERATE_SPEED;
			else
				m_fSpeedY *= 0.9f; //Speed decay

			if( keyboard.IsKeyDown( Keys.A ) )
				m_fSpeedX -= ACCELERATE_SPEED;
			else if( keyboard.IsKeyDown( Keys.D ) )
				m_fSpeedX += ACCELERATE_SPEED;
			else
				m_fSpeedX *= 0.9f; //Speed decay

			//Cap speed
			if( m_fSpeedX > MAX_SPEED )
				m_fSpeedX = MAX_SPEED;
			if( m_fSpeedX < MAX_SPEED * -1 )
				m_fSpeedX = MAX_SPEED * -1;
			if( m_fSpeedY > MAX_SPEED )
				m_fSpeedY = MAX_SPEED;
			if( m_fSpeedY < MAX_SPEED * -1 )
				m_fSpeedY = MAX_SPEED * -1;

			//Cap diagonal speed
			if( ( keyboard.IsKeyDown( Keys.W ) || keyboard.IsKeyDown( Keys.S ) ) && ( keyboard.IsKeyDown( Keys.A ) || keyboard.IsKeyDown( Keys.D ) ) )
			{
				if( m_fSpeedX > MAX_SPEED / 1.25f )
					m_fSpeedX = MAX_SPEED / 1.25f;
				if( m_fSpeedX < MAX_SPEED / 1.25f * -1 )
					m_fSpeedX = MAX_SPEED / 1.25f * -1;
				if( m_fSpeedY > MAX_SPEED / 1.25f )
					m_fSpeedY = MAX_SPEED / 1.25f;
				if( m_fSpeedY < MAX_SPEED / 1.25f * -1 )
					m_fSpeedY = MAX_SPEED / 1.25f * -1;
			}

			//Update position
			m_vPos += new Vector2( m_fSpeedX, m_fSpeedY );
			
			base.Update();
		}

		public override void Draw( SpriteBatch spriteBatch )
		{
			//Drawing player with angle property
			spriteBatch.Draw( m_Texture, m_vPos, null, Color.LawnGreen, m_fAngle, new Vector2( m_Texture.Width / 2, m_Texture.Height / 2 ), 1.0f, SpriteEffects.None, 0f );
		}
	}

	class Cursor
	{
		protected Texture2D m_Texture;
		protected Vector2 m_vPos;

		public Cursor( Texture2D texture )
		{
			m_Texture = texture;
			m_vPos = new Vector2( 0.0f, 0.0f );
		}

		public void Draw( SpriteBatch spriteBatch )
		{
			spriteBatch.Draw( m_Texture, m_vPos, Color.LawnGreen );
		}

		public void Update()
		{
			//Center cursor texture at the point of the mouse
			m_vPos.X = Mouse.GetState().X - m_Texture.Width / 2;
			m_vPos.Y = Mouse.GetState().Y - m_Texture.Height / 2;
		}
	}
}
